import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class HttpsRequestHandler implements HttpHandler {
	 public void handle(HttpExchange t) throws IOException {
         InputStream bis = new BufferedInputStream(t.getRequestBody());
         byte[] data = new byte[2048];
         int len = bis.read(data);
         System.out.printf("client received %d bytes: %s%n", len, new String(data, 0, len));
         
         String response = "This is the response";
         t.sendResponseHeaders(200, response.length());
         OutputStream os = t.getResponseBody();
         os.write(response.getBytes());
         os.flush();
     }
}
