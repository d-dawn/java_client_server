import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.SecureRandom;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

public class HttpsClient {
	public static void main(String []args){
		try {
			// Configure and build the HTTPS connection 
			URL url = new URL("https://localhost:8000/applications/myapp");
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			
			con.setRequestProperty("Content-Type", "text/xml; characterset=ISO-8859-1");
			con.setRequestProperty("SOAPAction", "\"\"");
			con.setConnectTimeout(10000);

			con.setRequestMethod("POST");       
			con.setDoInput(true);
			con.setDoOutput(true); 
			SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null, null, new SecureRandom());
			con.setSSLSocketFactory(sslContext.getSocketFactory());
			con.connect();
			
			// Load test XML
			String testXmlString = "NO DATA";
			@SuppressWarnings("resource")
			FileInputStream testXmlFile = new FileInputStream("../test.xml");
		    try {
		    	byte[] data = new byte[2048];
		    	int len = testXmlFile.read(data);
		    	
		    	testXmlString = new String(data, 0, len);
		    } catch (Exception e) {
		    	e.printStackTrace();
		    }
			
		    // Send the message to the web service
			OutputStream os = new BufferedOutputStream(con.getOutputStream());
//			String message = "Hello World Message";
//			System.out.println("sending message: " + message);
            System.out.println("sending message: " + testXmlString);
			
            os.write(testXmlString.getBytes());
            os.flush();

            // Receive message from the web service
			InputStream is = null;
			if (con.getResponseCode() < HttpsURLConnection.HTTP_BAD_REQUEST) {
			    is = con.getInputStream();
			} else {
			    is = con.getErrorStream();
			}
			
			InputStream bis = new BufferedInputStream(is);
            byte[] data = new byte[2048];
            int len = bis.read(data);
            System.out.printf("client received %d bytes: %s%n", len, new String(data, 0, len));
            
            is.close();
            os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
