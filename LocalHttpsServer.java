import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.security.SecureRandom;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;

import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;
import javax.net.ssl.KeyManagerFactory;

public class LocalHttpsServer {
	public static void main(String []args){
		try {
		    // Set up the socket address
		    InetSocketAddress address = new InetSocketAddress(8000);
	
		    // Initialize the HTTPS server
		    HttpsServer srv = HttpsServer.create(address, 0);
		    SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
		    
		    // Get the keystore
		    KeyStore ks = KeyStore.getInstance("JKS");
		    InputStream ksIs = new FileInputStream("H:/httpsserverkeystore.p12");
		    try {
		        ks.load(ksIs, "password".toCharArray());
		    } finally {
		        if (ksIs != null) {
		            ksIs.close();
		        }
		    }

		    KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory
		            .getDefaultAlgorithm());
		    kmf.init(ks, "password".toCharArray());
		    
		    // Init context with our keystore
		    sslContext.init(kmf.getKeyManagers(), null, new SecureRandom());
		    
		    // Configure HTTPS
		    srv.setHttpsConfigurator (new HttpsConfigurator(sslContext) {
		        public void configure (HttpsParameters params) {
			        SSLContext c = getSSLContext();
			        SSLEngine engine = c.createSSLEngine();
	                params.setNeedClientAuth(false);
	                params.setCipherSuites(engine.getEnabledCipherSuites());
	                params.setProtocols(engine.getEnabledProtocols());
	                
	                // get the default parameters
			        SSLParameters sslParams = c.getDefaultSSLParameters();
			        params.setSSLParameters(sslParams);
		        }
		    });
		    
		    // Create the request handler and start the web service
		    System.out.println("Starting web service...");
		    srv.createContext("/applications/myapp", new HttpsRequestHandler());
		    srv.setExecutor(null);
			srv.start();
			System.out.println("Web service started");
			System.out.println("Write requests to: https://localhost:8000/applications/myapp");
		} catch (Exception e) {
		    e.printStackTrace();
		}
	}
}
