import java.net.Socket;
import java.lang.Thread;
import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

public class ServerSocket {

	public static void main(String []args){
        try {
            ServerSocketFactory factory = SSLServerSocketFactory.getDefault();
            SSLServerSocket serverSocket = (SSLServerSocket) factory.createServerSocket(28960);
            serverSocket.setEnabledCipherSuites(serverSocket.getEnabledCipherSuites());
            serverSocket.setEnabledProtocols(new String[] { "TLSv1.2" });

            while(true) {
                System.out.println("Listening for messages...");
                Socket socket = serverSocket.accept();
                System.out.println("Client connected");
                Thread consumerThread = new ServerConsumerThread(socket);
                consumerThread.start();
            } 
        } catch (Exception e) {
            e.printStackTrace();
        }
     }
}