import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ServerConsumerThread extends Thread {
   Socket socket;

    public ServerConsumerThread(Socket socket) {
        this.socket = socket;
    }

    public void run(){
      try {
         System.out.println("ServerConsumerThread starting to process input");
         InputStream is = new BufferedInputStream(socket.getInputStream());
         byte[] data = new byte[2048];
         int len = is.read(data);
                
         String message = new String(data, 0, len);
         OutputStream os = new BufferedOutputStream(socket.getOutputStream());
         System.out.printf("server received %d bytes: %s%n", len, message);
         String response = message + " processed by server";
         os.write(response.getBytes(), 0, response.getBytes().length);
         os.flush();
         System.out.println("ServerConsumerThread flushed output");
      } catch (Exception e) {
         e.printStackTrace();
      }
    }
  }