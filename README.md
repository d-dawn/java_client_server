# java_client_server

## Name
Java Client Server Secure Communication

## Description
Java Client and Server Secure Socket communication example for Java 8 with TLSv1.2 utilizing standard socket and HTTPS connection.
Created this quick implementation only to do few local tests.

## Configuration
You have to create a keystore for the server and the self-signed cert itself. In addition you have to add the certification to the client's trust store which will be now the default trust store in this example.

### Run commands
#### Creating the keystore and key
keytool -genkey -alias serverkey -keyalg RSA -keysize 2048 -sigalg SHA256withRSA -keystore serverkeystore.p12 -storepass password -ext san=ip:127.0.0.1,dns:localhost

#### Exporting the certificate from the keystore
keytool -exportcert -keystore serverkeystore.p12 -alias serverkey -storepass password -rfc -file server-certificate.pem

#### Importing the certificate to the default trust store (using default store password yours could be different)
keytool -import -trustcacerts -file server-certificate.pem -keypass password -storepass changeit -keystore cacerts -alias serverkey

You can find the "cacerts" file in your JRE for example ..\com.sun.java.jdk8.win32.x86_64_1.8.0.u66\jre\lib\security

### VM argument on client side:
-Djavax.net.debug="all"

### VM arguments on server side:
-Djavax.net.ssl.keyStore="H:\\serverkeystore.p12"
-Djavax.net.ssl.keyStorePassword="password"
-Djavax.net.debug="all"

The certificate and the keystore can be used for both implementations. Make sure you change the references accordingly in the code and in the arguments too if you have the stores at a different location.

## Installation
Update the JRE security policy files with:
https://www.oracle.com/ae/java/technologies/javase-jce8-downloads.html

## Useful links
### HTTPS server
https://docs.oracle.com/javase/8/docs/jre/api/net/httpserver/spec/com/sun/net/httpserver/package-summary.html

### Server socket
https://docs.oracle.com/javase/10/security/sample-code-illustrating-secure-socket-connection-client-and-server.htm

## License
GNU General Public License, version 3
https://www.gnu.org/licenses/gpl-3.0.en.html
